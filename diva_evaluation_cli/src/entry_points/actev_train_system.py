"""Entry point module: train-system

Implements the entry-point by using Python or any other languages.
"""


def entry_point(activity_index, training_data_dir, verbose=False):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Train your system on an unknown activity. The command will read the
    contents of the activity index and train the system for the new activities.
    Data referenced to the activity-index is relative to the training data
    directory.

    Args:
        activity_index (str): Path to activity index json file
        training_data_dir (str): Path to training data directory
    """
    raise NotImplementedError("You should implement the entry_point method.")
