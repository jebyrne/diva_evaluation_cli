## Self-reported system output

Please use this folder to report your personal system outputs.
Create a subdirectory using the following pattern for its name:

```
<dataset-id>_*/
```


This subdirectory may then contain your system output using the following structure:

```
<dataset-id>_*/
    <dataset-id>_activity.json 		Activity index you used in this dataset
    <dataset-id>_file.json 			File index you used in this dataset
    <dataset-id>_chunk.json			Output of the command design-chunks you ran on this dataset
    <dataset-id>_output.json 		Output of the command merge-chunks you ran on this dataset
```
