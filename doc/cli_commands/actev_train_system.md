# actev train-system

## Description

Optional command where the system prepares to detect surprise activities (e.g., builds models).

This command requires the following parameters:
activity_index (str): Path to activity index json file
        training_data_dir (str): Path to training data directory

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|----------|----------------------------|
| activity-index     | a | True    | path to activity index json file   |
| training_data_dir  | t | True    | path to training data directory   |

## Usage

```bash
actev train-system -a <activity_index.json> -t <training_data_dir>
```
