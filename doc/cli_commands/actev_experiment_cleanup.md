# actev experiment-cleanup

## Description

Command that deletes any artifact produced by the previous execution of the system: cache files, output files, instances of containers... 

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|----------|----------------------------|
| system-cache-dir       | s | False    | path to system cache directory       |

## Usage

```
actev experiment-cleanup -s $SYSTEM_CACHE_DIR
```
